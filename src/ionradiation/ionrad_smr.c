#include "copyright.h"
/*============================================================================*/
/*! \file smr.c
 *  \brief Functions to handle static mesh refinement (SMR).
 *
 * PURPOSE: Functions to handle static mesh refinement (SMR).
 *
 * REFERENCES:
 * - G. Toth and P.L. Roe, "Divergence and Curl-preserving prolongation and
 *   restriction formulas", JCP 180, 736 (2002)
 *
 * CONTAINS PUBLIC FUNCTIONS:
 * - RestrictCorrect(): restricts (averages) fine Grid solution to coarse, and
 *    corrects cells at fine/coarse boundaries using restricted fine Grid fluxes
 * - Prolongate(): sets BC on fine Grid by prolongation (interpolation) of
 *     coarse Grid solution into fine grid ghost zones
 * - SMR_init(): allocates memory for send/receive buffers
 *
 * PRIVATE FUNCTION PROTOTYPES:
 * - ProCon() - prolongates conserved variables
 * - ProFld() - prolongates face-centered B field using TR formulas
 * - mcd_slope() - returns monotonized central-difference slope		      */
/*============================================================================*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include "../defs.h"
#include "../athena.h"
#include "../prototypes.h"
#include "prototypes.h"
#include "ionrad.h"

#ifdef STATIC_MESH_REFINEMENT

static double **send_bufP = NULL;
static double **send_bufRC = NULL;
static double **recv_bufP = NULL;
#ifdef MPI_PARALLEL
static double ***recv_bufRC = NULL;
static MPI_Request **recv_rq = NULL;
static MPI_Request **send_rq = NULL;
#endif
static int maxND, *start_addrP;
/* number of ion variables passed */
#define NION 1
static Real ***GZ[3];

/*==============================================================================
 * PRIVATE FUNCTION PROTOTYPES:
 *   ProFlx - prolongates ionrad variables
 *============================================================================*/
void ProFlx(const Real Fi,
            const Real Fim1, const Real Fip1,
            const Real Fjm1, const Real Fjp1,
            const Real Fkm1, const Real Fkp1, Real PFlx[][2][2]);

/*=========================== PUBLIC FUNCTIONS ===============================*/
/*----------------------------------------------------------------------------*/
/*! \fn void RestrictCorrect(MeshS *pM)
 *  \brief Restricts (averages) fine Grid solution to coarse, and
 *    corrects cells at fine/coarse boundaries using restricted fine Grid fluxes
 */

void RestrictCorrect_ionrad(MeshS *pM){
  GridS *pG;
  int nl, nd, ncg, dim, nDim, npg, start_addr, cnt, nCons, nFlx, nZeroRC;
  int i, ics, ice, ips, ipe;
  int j, jcs, jce, jps, jpe;
  int k, kcs, kce, kps, kpe;
  int nSnd;
  Real fact;
  double *pRcv, *pSnd;
  GridOvrlpS *pCO, *pPO;

#if (NSCALARS > 0)
  int n;
#endif
#ifdef MPI_PARALLEL
  int ierr, mAddress, mIndex, mCount, nRcv, rbufN;
#endif

/* number of dimensions in Grid. */
  nDim = 1;
  for (i = 1; i < 3; i++) {
    if (pM->Nx[i] > 1) {
      nDim++;
    }
  }

/* Loop over all Domains, starting at maxlevel */

  for (nl = (pM->NLevels)-1; nl >= 0; nl--) {
#ifdef MPI_PARALLEL
/* Post non-blocking receives at level nl-1 for data from child Grids at this
 * level (nl).  This data is sent in Step 3 below, and will be read in Step 1
 * at the next iteration of the loop. */

    if (nl > 0) {
      for (nd = 0; nd < (pM->DomainsPerLevel[nl-1]); nd++) {
        if (pM->Domain[nl-1][nd].Grid != NULL) {
          pG = pM->Domain[nl-1][nd].Grid;
          nZeroRC = 0;

/* Recv buffer is addressed from 0 for first MPI message, even if NmyCGrid>0.
 * First index alternates between 0 and 1 for even/odd values of nl, since if
 * there are Grids on multiple levels there may be 2 receives posted at once */
          mAddress = 0;
          rbufN = ((nl-1)%2);
          for (ncg = (pG->NmyCGrid); ncg < (pG->NCGrid); ncg++) {
            nRcv = (pG->CGrid[ncg].nWordsRC/NVAR)*(1+NSCALARS);
            if (pG->CGrid[ncg].nWordsRC == 0) {
              nZeroRC += 1;
            }
            else {
              mIndex = ncg-pG->NmyCGrid-nZeroRC;
              ierr   = MPI_Irecv(&(recv_bufRC[rbufN][nd][mAddress]),
                                 nRcv, MPI_DOUBLE,
                                 pG->CGrid[ncg].ID,
                                 pG->CGrid[ncg].DomN,
                                 pM->Domain[nl-1][nd].Comm_Children,
                                 &(recv_rq[nd][mIndex]));
              mAddress += nRcv;
            }
          }
        }
      }
    }
#endif /* MPI_PARALLEL */

/*=== Step 1. Get child solution, inject into parent Grid ====================*/
/* Loop over Domains and child Grids.  Maxlevel domains skip this step because
 * they have NCGrids=0 */

    for (nd = 0; nd < (pM->DomainsPerLevel[nl]); nd++) {
      if (pM->Domain[nl][nd].Grid != NULL) { /* there is a Grid on this processor */
        pG = pM->Domain[nl][nd].Grid;
        nZeroRC = 0;

        for (i = pG->NmyCGrid; i < pG->NCGrid; i++) {
          if (pG->CGrid[i].nWordsRC == 0) {
            nZeroRC++;
          }
        }

        for (ncg = 0; ncg < (pG->NCGrid-nZeroRC); ncg++) {
/*--- Step 1a. Get restricted solution and fluxes. ---------------------------*/

/* If child Grid is on this processor, set pointer to start of send buffer
 * loaded by this child Grid in Step 3 below during last iteration of loop. */

          if (ncg < pG->NmyCGrid) {
            pCO  = (GridOvrlpS*)&(pG->CGrid[ncg]);
            pRcv = (double*)&(send_bufRC[pCO->DomN][0]);
          }
          else {
#ifdef MPI_PARALLEL
/* Check non-blocking receives posted above for restricted solution from child
 * Grids, sent in Step 3 during last iteration of loop over nl.  Accept messages
 * in any order. */
            rbufN  = (nl%2);
            mCount = pG->NCGrid-pG->NmyCGrid-nZeroRC;
            ierr   = MPI_Waitany(mCount, recv_rq[nd], &mIndex,
                                 MPI_STATUS_IGNORE);
            if (mIndex == MPI_UNDEFINED) {
              ath_error("[RestCorr]: Invalid request index nl=%i nd=%i\n", nl,
                        nd);
            }

/* Recv buffer is addressed from 0 for first MPI message, even if NmyCGrid>0 */

            mIndex += pG->NmyCGrid;

            for (i = pG->NmyCGrid; i <= mIndex; i++) {
              if (pG->CGrid[i].nWordsRC == 0) {
                mIndex++;
              }
            }

            mAddress = 0;
            for (i = pG->NmyCGrid; i < mIndex; i++) {
              mAddress += (pG->CGrid[i].nWordsRC/NVAR)*(1+NSCALARS);
            }
            pCO  = (GridOvrlpS*)&(pG->CGrid[mIndex]);
            pRcv = (double*)&(recv_bufRC[rbufN][nd][mAddress]);
#else
/* If not MPI_PARALLEL, and child Grid not on this processor, then error */

            ath_error("[RestCorr]: no Child grid on Domain[%d][%d]\n", nl, nd);
#endif /* MPI_PARALLEL */
          }

/* Get coordinates ON THIS GRID of overlap region of child Grid */

          ics = pCO->ijks[0];
          ice = pCO->ijke[0];
          jcs = pCO->ijks[1];
          jce = pCO->ijke[1];
          kcs = pCO->ijks[2];
          kce = pCO->ijke[2];

/*--- Step 1b. Restrict conserved variables on parent Grid -------------------*/

/* If no restriction is required, kce < kcs, the loop will not be calculated */
          for (k = kcs; k <= kce; k++) {
            for (j = jcs; j <= jce; j++) {
              for (i = ics; i <= ice; i++) {
#ifndef BAROTROPIC
                pG->U[k][j][i].E = *(pRcv++);
#endif /* BAROTROPIC */
#if (NSCALARS > 0)
                for (n = 0; n < NSCALARS; n++) {
                  pG->U[k][j][i].s[n] = *(pRcv++);
                }
#endif
              }
            }
          }
        } /* end loop over child grids */
      }
    } /* end loop over Domains */

/*=== Step 3. Restrict child solution and fluxes and send ====================*/
/* Loop over all Domains and parent Grids.  Maxlevel grids skip straight to this
 * step to start the chain of communication.  Root (level=0) skips this step
 * since it has NPGrid=0.  If there is a parent Grid on this processor, it will
 * be first in the PGrid array, so it will be at start of send_bufRC */

    for (nd = 0; nd < (pM->DomainsPerLevel[nl]); nd++) {
      if (pM->Domain[nl][nd].Grid != NULL) { /* there is a Grid on this processor */
        pG = pM->Domain[nl][nd].Grid;    /* set pointer to this Grid */
        start_addr = 0;
        nZeroRC = 0;

        for (npg = 0; npg < (pG->NPGrid); npg++) {
          if (pG->PGrid[npg].nWordsRC == 0) {
            if (npg >= pG->NmyPGrid) {
              nZeroRC += 1;
            }
          }
          else{
            pPO = (GridOvrlpS*)&(pG->PGrid[npg]);  /* ptr to Grid overlap */
            cnt = 0;

/* Get coordinates ON THIS GRID of overlap region of parent Grid */

            ips = pPO->ijks[0];
            ipe = pPO->ijke[0];
            jps = pPO->ijks[1];
            jpe = pPO->ijke[1];
            kps = pPO->ijks[2];
            kpe = pPO->ijke[2];

/*--- Step 3a. Restrict conserved variables  ---------------------------------*/
/* 1D/2D/3D problem: Conservative average of conserved variables in x1. */

            pSnd = (double*)&(send_bufRC[nd][start_addr]);
            for (k = kps; k <= kpe; k += 2) {
              for (j = jps; j <= jpe; j += 2) {
                for (i = ips; i <= ipe; i += 2) {
#ifndef BAROTROPIC
                  *(pSnd++) = pG->U[k][j][i].E+pG->U[k][j][i+1].E;
#endif
#if (NSCALARS > 0)
                  for (n = 0; n < NSCALARS; n++) {
                    *(pSnd++) = pG->U[k][j][i].s[n]+pG->U[k][j][i+1].s[n];
                  }
#endif
                }
              }
            }
            fact  = 0.5;
            nCons = (ipe-ips+1)*(1+NSCALARS)/2;

/* 2D/3D problem: Add conservative average in x2 */

            if (pG->Nx[1] > 1) {
              pSnd = (double*)&(send_bufRC[nd][start_addr]); /* restart pointer */
              for (k = kps; k <= kpe; k += 2) {
                for (j = jps; j <= jpe; j += 2) {
                  for (i = ips; i <= ipe; i += 2) {
#ifndef BAROTROPIC
                    *(pSnd++) += pG->U[k][j+1][i].E+pG->U[k][j+1][i+1].E;
#endif
#if (NSCALARS > 0)
                    for (n = 0; n < NSCALARS; n++) {
                      *(pSnd++) += pG->U[k][j+1][i].s[n]+
                                   pG->U[k][j+1][i+1].s[n];
                    }
#endif
                  }
                }
              }
              fact  = 0.25;
              nCons = (jpe-jps+1)*(ipe-ips+1)*(1+NSCALARS)/4;
            }

/* 3D problem: Add conservative average in x3 */

            if (pG->Nx[2] > 1) {
              pSnd = (double*)&(send_bufRC[nd][start_addr]); /* restart pointer */
              for (k = kps; k <= kpe; k += 2) {
                for (j = jps; j <= jpe; j += 2) {
                  for (i = ips; i <= ipe; i += 2) {
#ifndef BAROTROPIC
                    *(pSnd++) += pG->U[k+1][j  ][i].E+pG->U[k+1][j  ][i+1].E+
                                 pG->U[k+1][j+1][i].E+pG->U[k+1][j+1][i+1].E;
#endif
#if (NSCALARS > 0)
                    for (n = 0; n < NSCALARS; n++) {
                      *(pSnd++) += pG->U[k+1][j  ][i].s[n]+
                                   pG->U[k+1][j  ][i+1].s[n]+
                                   pG->U[k+1][j+1][i].s[n]+
                                   pG->U[k+1][j+1][i+1].s[n];
                    }
#endif
                  }
                }
              }
              fact  = 0.125;
              nCons = (kpe-kps+1)*(jpe-jps+1)*(ipe-ips+1)*(1+NSCALARS)/8;
            }

/* reset pointer to beginning and normalize averages */
            pSnd = (double*)&(send_bufRC[nd][start_addr]);
            for (i = start_addr; i < (start_addr+nCons); i++) {
              *(pSnd++) *= fact;
            }
            cnt = nCons;

/*--- Step 3c. Restrict fluxes of conserved variables ------------------------*/
/*---------------- Restrict fluxes at x1-faces -------------------------------*/

            for (dim = 0; dim < 2; dim++) {
              if (pPO->myFlx[dim] != NULL) {
                pSnd = (double*)&(send_bufRC[nd][(start_addr+cnt)]);

                if (nDim == 1) { /*----- 1D problem -----*/
#ifndef BAROTROPIC
                  *(pSnd++) = pPO->myFlx[dim][kps][jps].E;
#endif
#if (NSCALARS > 0)
                  for (n = 0; n < NSCALARS; n++) {
                    *(pSnd++) = pPO->myFlx[dim][kps][jps].s[n];
                  }
#endif
                  nFlx = 1+NSCALARS;
                }
                else { /*----- 2D or 3D problem -----*/
/* Conservative average in x2 of x1-fluxes */

                  for (k = 0; k <= (kpe-kps); k += 2) {
                    for (j = 0; j <= (jpe-jps); j += 2) {
#ifndef BAROTROPIC
                      *(pSnd++) = pPO->myFlx[dim][k][j].E+
                                  pPO->myFlx[dim][k][j+1].E;
#endif
#if (NSCALARS > 0)
                      for (n = 0; n < NSCALARS; n++) {
                        *(pSnd++) =
                          pPO->myFlx[dim][k][j].s[n]+
                          pPO->myFlx[dim][k][j+1].s[n];
                      }
#endif
                    }
                  }
                  fact = 0.5;
                  nFlx = ((jpe-jps+1)/2)*(1+NSCALARS);

/* Add conservative average in x3 of x1-fluxes */

                  if (nDim == 3) { /*----- 3D problem -----*/
                    pSnd = (double*)&(send_bufRC[nd][start_addr+cnt]); /* restart ptr */
                    for (k = 0; k <= (kpe-kps); k += 2) {
                      for (j = 0; j <= (jpe-jps); j += 2) {
#ifndef BAROTROPIC
                        *(pSnd++) += pPO->myFlx[dim][k+1][j].E+
                                     pPO->myFlx[dim][k+1][j+1].E;
#endif
#if (NSCALARS > 0)
                        for (n = 0; n < NSCALARS; n++) {
                          *(pSnd++) +=
                            pPO->myFlx[dim][k+1][j].s[n]+
                            pPO->myFlx[dim][k+1][j+1].s[n];
                        }
#endif
                      }
                    }
                    fact = 0.25;
                    nFlx = ((kpe-kps+1)*(jpe-jps+1)/4)*(1+NSCALARS);
                  }

/* reset pointer to beginning of x1-fluxes and normalize averages */
                  pSnd = (double*)&(send_bufRC[nd][(start_addr+cnt)]);
                  for (i = (start_addr+cnt); i < (start_addr+cnt+nFlx); i++) {
                    *(pSnd++) *= fact;
                  }
                }
                cnt += nFlx;
              }
            }

/*---------------- Restrict fluxes at x2-faces -------------------------------*/

            for (dim = 2; dim < 4; dim++) {
              if (pPO->myFlx[dim] != NULL) {
                pSnd = (double*)&(send_bufRC[nd][(start_addr+cnt)]);

/* Conservative average in x1 of x2-fluxes */

                for (k = 0; k <= (kpe-kps); k += 2) {
                  for (i = 0; i <= (ipe-ips); i += 2) {
#ifndef BAROTROPIC
                    *(pSnd++) = pPO->myFlx[dim][k][i].E+
                                pPO->myFlx[dim][k][i+1].E;
#endif
#if (NSCALARS > 0)
                    for (n = 0; n < NSCALARS; n++) {
                      *(pSnd++) =
                        pPO->myFlx[dim][k][i].s[n]+pPO->myFlx[dim][k][i+1].s[n];
                    }
#endif
                  }
                }
                fact = 0.5;
                nFlx = ((ipe-ips+1)/2)*(1+NSCALARS);

/* Add conservative average in x3 of x2-fluxes */

                if (nDim == 3) { /*----- 3D problem -----*/
                  pSnd = (double*)&(send_bufRC[nd][start_addr+cnt]); /* restart ptr */
                  for (k = 0; k <= (kpe-kps); k += 2) {
                    for (i = 0; i <= (ipe-ips); i += 2) {
#ifndef BAROTROPIC
                      *(pSnd++) += pPO->myFlx[dim][k+1][i].E+
                                   pPO->myFlx[dim][k+1][i+1].E;
#endif
#if (NSCALARS > 0)
                      for (n = 0; n < NSCALARS; n++) {
                        *(pSnd++) +=
                          pPO->myFlx[dim][k+1][i].s[n]+
                          pPO->myFlx[dim][k+1][i+1].s[n];
                      }
#endif
                    }
                  }
                  fact = 0.25;
                  nFlx = ((kpe-kps+1)*(ipe-ips+1)/4)*(1+NSCALARS);
                }

/* reset pointer to beginning of x2-fluxes and normalize averages */
                pSnd = (double*)&(send_bufRC[nd][(start_addr+cnt)]);
                for (i = (start_addr+cnt); i < (start_addr+cnt+nFlx); i++) {
                  *(pSnd++) *= fact;
                }
                cnt += nFlx;
              }
            }

/*---------------- Restrict fluxes at x3-faces -------------------------------*/

            for (dim = 4; dim < 6; dim++) {
              if (pPO->myFlx[dim] != NULL) {
                pSnd = (double*)&(send_bufRC[nd][(start_addr+cnt)]);

/* Conservative average in x1 of x3-fluxes */

                for (j = 0; j <= (jpe-jps); j += 2) {
                  for (i = 0; i <= (ipe-ips); i += 2) {
#ifndef BAROTROPIC
                    *(pSnd++) = pPO->myFlx[dim][j][i].E+
                                pPO->myFlx[dim][j][i+1].E;
#endif
#if (NSCALARS > 0)
                    for (n = 0; n < NSCALARS; n++) {
                      *(pSnd++) =
                        pPO->myFlx[dim][j][i].s[n]+pPO->myFlx[dim][j][i+1].s[n];
                    }
#endif
                  }
                }

/* Add conservative average in x2 of x3-fluxes */

                pSnd = (double*)&(send_bufRC[nd][(start_addr+cnt)]);
                for (j = 0; j <= (jpe-jps); j += 2) {
                  for (i = 0; i <= (ipe-ips); i += 2) {
#ifndef BAROTROPIC
                    *(pSnd++) += pPO->myFlx[dim][j+1][i].E+
                                 pPO->myFlx[dim][j+1][i+1].E;
#endif
#if (NSCALARS > 0)
                    for (n = 0; n < NSCALARS; n++) {
                      *(pSnd++) +=
                        pPO->myFlx[dim][j+1][i].s[n]+
                        pPO->myFlx[dim][j+1][i+1].s[n];
                    }
#endif
                  }
                }
                fact = 0.25;
                nFlx = ((jpe-jps+1)*(ipe-ips+1)/4)*(1+NSCALARS);

/* reset pointer to beginning of x3-fluxes and normalize averages */
                pSnd = (double*)&(send_bufRC[nd][(start_addr+cnt)]);
                for (i = (start_addr+cnt); i < (start_addr+cnt+nFlx); i++) {
                  *(pSnd++) *= fact;
                }
                cnt += nFlx;
              }
            }
            nSnd = (pG->PGrid[npg].nWordsRC/NVAR)*(1+NSCALARS);
#ifdef MPI_PARALLEL
/*--- Step 3e. Send rectricted soln and fluxes -------------------------------*/
/* non-blocking send with MPI, using Domain number as tag.  */

            if (npg >= pG->NmyPGrid) {
              mIndex = npg-pG->NmyPGrid-nZeroRC;
              ierr   = MPI_Isend(&(send_bufRC[nd][start_addr]),
                                 nSnd,
                                 MPI_DOUBLE, pG->PGrid[npg].ID, nd,
                                 pM->Domain[nl][nd].Comm_Parent,
                                 &(send_rq[nd][mIndex]));
            }
#endif /* MPI_PARALLEL */

            start_addr += nSnd;
          }/* End if nWordsRC != 0 */
        } /* end loop over parent grids */
      }
    } /* end loop over Domains per level */

#ifdef MPI_PARALLEL
/*--- Step 4. Check non-blocking sends completed. ----------------------------*/
/* For MPI jobs, wait for all non-blocking sends in Step 3e to complete.  This
 * is more efficient if there are multiple messages per Grid. */

    for (nd = 0; nd < (pM->DomainsPerLevel[nl]); nd++) {
      if (pM->Domain[nl][nd].Grid != NULL) {
        pG = pM->Domain[nl][nd].Grid;
        nZeroRC = 0;

        for (i = pG->NmyPGrid; i < pG->NPGrid; i++) {
          if (pG->PGrid[i].nWordsRC == 0) {
            nZeroRC++;
          }
        }

        if (pG->NPGrid > pG->NmyPGrid) {
          mCount = pG->NPGrid-pG->NmyPGrid-nZeroRC;
          ierr   = MPI_Waitall(mCount, send_rq[nd], MPI_STATUS_IGNORE);
        }
      }
    }
#endif /* MPI_PARALLEL */
  } /* end loop over levels */

  return;
}

/*============================================================================*/
/*----------------------------------------------------------------------------*/
/*! \fn void Prolongate(MeshS *pM)
 *  \brief Sets BC on fine Grid by prolongation (interpolation) of
 *     coarse Grid solution into fine grid ghost zones */

/*=== Step 1. Send step ======================================================*/
/* Send flux to all child grid ghost zones. */
void Prolongate_ionrad_snd(DomainS *pD){
  GridS *pG = pD->Grid;
  unsigned int n;
  int dim, dir, ncg, nSnd;
  int i, ics, ice;
  int j, jcs, jce;
  int k, kcs, kce;
  double *pSnd;
  GridOvrlpS *pCO;

#ifdef MPI_PARALLEL
  int ierr, mIndex, nZeroP = 0;
#endif

  for (i = 0; i < maxND; i++) {
    start_addrP[i] = 0;
  }

  for (ncg = 0; ncg < (pG->NCGrid); ncg++) {
    /* Skip if no prolongation needed for this child (only flux correction) */
    if (pG->CGrid[ncg].nWordsP == 0) {
#ifdef MPI_PARALLEL
      if (ncg >= pG->NmyCGrid) {
        nZeroP += 1;
      }
#endif
    }
    else {
      pCO  = (GridOvrlpS*)&(pG->CGrid[ncg]); /* ptr to child Grid overlap */
      /* index send_buf with DomN of child, since could be multiple child Domains on
       * same processor.  Start address must be different for each DomN */
      pSnd = (double*)&(send_bufP[pCO->DomN][start_addrP[pCO->DomN]]);
      nSnd = (pCO->nWordsP/NVAR)*NION*pG->nradplane;
      for (n = 0; n < pG->nradplane; n++) {
        dir = pG->radplanelist[n].dir;
        dim = dir < 0 ? -2*(dir+1) : 2*dir-1;
        if (pCO->myFlx[dim] != NULL) {
          /* Get coordinates ON THIS GRID of zones that overlap child Grid ghost zones */
          ics = pCO->ijks[0]-(nghost/2)-1;
          ice = pCO->ijke[0]+(nghost/2)+1;
          if (pG->Nx[1] > 1) {
            jcs = pCO->ijks[1]-(nghost/2)-1;
            jce = pCO->ijke[1]+(nghost/2)+1;
          }
          else {
            jcs = pCO->ijks[1];
            jce = pCO->ijke[1];
          }
          if (pG->Nx[2] > 1) {
            kcs = pCO->ijks[2]-(nghost/2)-1;
            kce = pCO->ijke[2]+(nghost/2)+1;
          }
          else {
            kcs = pCO->ijks[2];
            kce = pCO->ijke[2];
          }
          if (dim == 0) {
            ice = pCO->ijks[0];
          }
          if (dim == 1) {
            ics = pCO->ijke[0];
          }
          if (dim == 2) {
            jce = pCO->ijks[1];
          }
          if (dim == 3) {
            jcs = pCO->ijke[1];
          }
          if (dim == 4) {
            kce = pCO->ijks[2];
          }
          if (dim == 5) {
            kcs = pCO->ijke[2];
          }
/*--- Step 1a. ---------------------------------------------------------------*/
/* Load send buffer with values in zones that overlap child ghost zones */
/* Make sure # variables loaded in a single loop iter equals NION set in init */
          for (k = kcs; k <= kce; k++) {
            for (j = jcs; j <= jce; j++) {
              for (i = ics; i <= ice; i++) {
                *(pSnd++) = pG->radplanelist[n].CFlux[k][j][i];
              }
            }
          }
        } /* loop over radplanes */
      }
/*--- Step 1b. ---------------------------------------------------------------*/
/* non-blocking send of data to child, using Domain number as tag. */
#ifdef MPI_PARALLEL
      if (ncg >= pG->NmyCGrid) {
        mIndex = ncg-pG->NmyCGrid-nZeroP;
        ierr   = MPI_Isend(
          &(send_bufP[pCO->DomN][start_addrP[pCO->DomN]]),
          nSnd, MPI_DOUBLE,
          pCO->ID, pD->DomNumber,
          pD->Comm_Children,
          &(send_rq[pD->DomNumber][mIndex]));
      }
#endif /* MPI_PARALLEL */
      start_addrP[pCO->DomN] += nSnd;
    } /* end if/else on nWordsP */
  } /* end loop over child grids */

  return;
}

/*=== Step 2. Get step =======================================================*/
/* Loop over all Domains, get data sent by parent Grids, and prolongate solution
 * into ghost zones. */
void Prolongate_ionrad_rcv(DomainS *pD){
  GridS *pG = pD->Grid;
  unsigned int r;
  int nZeroP, nRcv;
  int npg, ncg, nDim, dim, dir, id;
  int l, m, n;
  int i, ii, ips, ipe, igzs, igze;
  int j, jj, jps, jpe, jgzs, jgze;
  int k, kk, kps, kpe, kgzs, kgze;
  int ngz1, ngz2, ngz3, mend, nend;
  double *pRcv;
  GridOvrlpS *pPO, *pCO;
  Real ProlongedF[2][2][2];

#ifdef MPI_PARALLEL
  int ierr, mAddress, mIndex, mCount;
#endif

/*=== Step 4. Clear send_bufP ================================================*/
/* For each Domain nd, the data for child grid on same processor must be in
 * first element of CGrid array */
  for (npg = 0; npg < (pG->NmyPGrid); npg++) {
    pPO = (GridOvrlpS*)&(pG->PGrid[npg]); /* ptr to parent grid overlap */
    for (r = 0; r < (pPO->nWordsP/NVAR)*NION*pG->nradplane; r++) {
      recv_bufP[pPO->DomN][r] = send_bufP[pPO->DomN][r];
    }
  }

  /* number of dimensions in Grid. */
  nDim = 1;
  for (dim = 1; dim < 3; dim++) {
    if (pG->Nx[dim] > 1) {
      nDim++;
    }
  }

#ifdef MPI_PARALLEL
  /* Post non-blocking receives data from parent grids. This data is sent
   * in Prolong_ionrad_snd. */
  /* Recv buffer is addressed from PGrid[0].nWordsP if NmyPGrid>0 since data
   * from parent Grid on same processor begins at 0 (see Step 4 below). */
  nZeroP   = 0;
  mAddress = 0;
  if (pG->NmyPGrid > 0) {
    mAddress = (pG->PGrid[0].nWordsP/NVAR)*NION*pG->nradplane;
  }
  for (npg = (pG->NmyPGrid); npg < (pG->NPGrid); npg++) {
    /* Skip if no prolongation needed for this child (only flux correction) */
    if (pG->PGrid[npg].nWordsP == 0) {
      nZeroP += 1;
    }
    else {
      nRcv   = (pG->PGrid[npg].nWordsP/NVAR)*NION*pG->nradplane;
      mIndex = npg-pG->NmyPGrid-nZeroP;
      ierr   = MPI_Irecv(&(recv_bufP[pD->DomNumber][mAddress]),
                         nRcv, MPI_DOUBLE,
                         pG->PGrid[npg].ID,
                         pG->PGrid[npg].DomN,
                         pD->Comm_Parent,
                         &(recv_rq[pD->DomNumber][mIndex]));
      mAddress += nRcv;
    }
  }
#endif /* MPI_PARALLEL */

  /* Loop over number of parent grids with non-zero-size prolongation data */
  nZeroP = 0;
  for (i = pG->NmyPGrid; i < pG->NPGrid; i++) {
    if (pG->PGrid[i].nWordsP == 0) {
      nZeroP++;
    }
  }

  for (npg = 0; npg < (pG->NPGrid-nZeroP); npg++) {
    /* If parent Grid is on this processor, data is at start of recv buffer */
    if (npg < pG->NmyPGrid) {
      pPO  = (GridOvrlpS*)&(pG->PGrid[npg]);
      pRcv = (double*)&(recv_bufP[pD->DomNumber][0]);
    }
    else {
#ifdef MPI_PARALLEL
      /* Check non-blocking receives posted above for data in ghost zone from parent
       * Grids, sent in Step 1.  Accept messages in any order. */
      mCount = pG->NPGrid-pG->NmyPGrid-nZeroP;
      ierr   = MPI_Waitany(mCount, recv_rq[pD->DomNumber], &mIndex,
                           MPI_STATUS_IGNORE);
      if (mIndex == MPI_UNDEFINED) {
        ath_error("[Prolong]: Invalid request index nd=%i\n", pD->DomNumber);
      }
      /* Recv buffer is addressed from PGrid[0].nWordsP for first MPI message
       * if NmyPGrid>0.  Also must remove zero size messages from index. */
      mIndex += pG->NmyPGrid;
      for (i = pG->NmyPGrid; i <= mIndex; i++) {
        if (pG->PGrid[i].nWordsP == 0) {
          mIndex++;
        }
      }
      mAddress = 0;
      for (i = 0; i < mIndex; i++) {
        mAddress += (pG->PGrid[i].nWordsP/NVAR)*NION*pG->nradplane;
      }
      pPO  = (GridOvrlpS*)&(pG->PGrid[mIndex]);
      pRcv = (double*)&(recv_bufP[pD->DomNumber][mAddress]);
#else
      /* If not MPI_PARALLEL, and parent Grid not on this processor, then error */
      ath_error("[Prolong]: no Parent Grid on Domain[%d][%d]\n", pD->Level,
                pD->DomNumber);
#endif /* MPI_PARALLEL */
    }
/*=== Step 3. Set ghost zones ================================================*/
    /* Loop over 6 boundaries, set ghost zones */
    for (r = 0; r < pG->nradplane; r++) {
      dir = pG->radplanelist[r].dir;
      dim = dir < 0 ? -2*(dir+1) : 2*dir-1;
      if ((pPO->myFlx[dim] != NULL) && (pPO->nWordsP > 0)) {
/*--- Steps 3a. Set GZ -------------------------------------------------------*/
        /* Compute size of array containing ghost zones from parent Grid.  Set
         * starting and ending indices of GZ array. */
        if (dim == 0 || dim == 1) {
          ngz1 = (nghost/2)+2;
          id   = 0;
        }
        else {
          ngz1 = (pPO->ijke[0]-pPO->ijks[0]+1)/2+nghost+2;
        }
        if (dim == 2 || dim == 3) {
          ngz2 = (nghost/2)+2;
          id   = 1;
        }
        else {
          ngz2 = (pPO->ijke[1]-pPO->ijks[1]+1)/2+nghost+2;
        }
        if (dim == 4 || dim == 5) {
          ngz3 = (nghost/2)+2;
          id   = 2;
        }
        else {
          ngz3 = (pPO->ijke[2]-pPO->ijks[2]+1)/2+nghost+2;
        }
        igzs = 0;
        igze = ngz1-1;
        if (pG->Nx[1] > 1) {
          jgzs = 0;
          jgze = ngz2-1;
          mend = 1;
        }
        else {
          ngz2 = 1;
          jgzs = 1;
          jgze = 1;
          mend = 0;
        }
        if (pG->Nx[2] > 1) {
          kgzs = 0;
          kgze = ngz3-1;
          nend = 1;
        }
        else {
          ngz3 = 1;
          kgzs = 1;
          kgze = 1;
          nend = 0;
        }
        /* Load GZ array with values in receive buffer */
        for (k = kgzs; k <= kgze; k++) {
          for (j = jgzs; j <= jgze; j++) {
            for (i = igzs; i <= igze; i++) {
              GZ[id][k][j][i] = *(pRcv++);
            }
          }
        }
        /* Set BC on GZ array in 1D and 2D */
        if (nDim == 1) {
          for (i = igzs; i <= igze; i++) {
            GZ[id][1][0][i] = GZ[id][1][1][i];
            GZ[id][1][2][i] = GZ[id][1][1][i];
            GZ[id][0][1][i] = GZ[id][1][1][i];
            GZ[id][2][1][i] = GZ[id][1][1][i];
          }
        }
        else if (nDim == 2) {
          for (j = jgzs; j <= jgze; j++) {
            for (i = igzs; i <= igze; i++) {
              GZ[id][0][j][i] = GZ[id][1][j][i];
              GZ[id][2][j][i] = GZ[id][1][j][i];
            }
          }
        }
/*--- Steps 3b.  Prolongate cell-centered values -----------------------------*/
/* Get coordinates ON THIS GRID of ghost zones that overlap parent Grid */
        ips = pPO->ijks[0]-nghost;
        ipe = pPO->ijke[0]+nghost;
        if (pG->Nx[1] > 1) {
          jps = pPO->ijks[1]-nghost;
          jpe = pPO->ijke[1]+nghost;
        }
        else {
          jps = pPO->ijks[1];
          jpe = pPO->ijke[1];
        }
        if (pG->Nx[2] > 1) {
          kps = pPO->ijks[2]-nghost;
          kpe = pPO->ijke[2]+nghost;
        }
        else {
          kps = pPO->ijks[2];
          kpe = pPO->ijke[2];
        }
        if (dim == 0) {
          ipe = pPO->ijks[0]-1;
        }
        if (dim == 1) {
          ips = pPO->ijke[0]+1;
        }
        if (dim == 2) {
          jpe = pPO->ijks[1]-1;
        }
        if (dim == 3) {
          jps = pPO->ijke[1]+1;
        }
        if (dim == 4) {
          kpe = pPO->ijks[2]-1;
        }
        if (dim == 5) {
          kps = pPO->ijke[2]+1;
        }
/* Prolongate these values in ghost zones */
        for (k = kps, kk = 1; k <= kpe; k += 2, kk++) {
          for (j = jps, jj = 1; j <= jpe; j += 2, jj++) {
            for (i = ips, ii = 1; i <= ipe; i += 2, ii++) {
              ProFlx(GZ[id][kk][jj][ii],
                     GZ[id][kk][jj][ii-1], GZ[id][kk][jj][ii+1],
                     GZ[id][kk][jj-1][ii], GZ[id][kk][jj+1][ii],
                     GZ[id][kk-1][jj][ii], GZ[id][kk+1][jj][ii],
                     ProlongedF);
/* 1D/2D/3D problem, set solution prolongated in x1 */
              for (n = 0; n <= nend; n++) {
                for (m = 0; m <= mend; m++) {
                  for (l = 0; l <= 1; l++) {
                    pG->radplanelist[r].CFlux[k+n][j+m][i+l] =
                      ProlongedF[n][m][l];
                  }
                }
              } /* end loop filling grid fluxes */
            }
          }
        } /* end loop over prolongation */
      } /* end if words to pass */
    } /* end loop over radplanes */
  } /* end loop over parent grids */

  return;
}

/*============================================================================*/
/*----------------------------------------------------------------------------*/
/*! \fn void SMR_init(MeshS *pM)
 *  \brief Allocates memory for send/receive buffers
 */

void SMR_ionrad_init(MeshS *pM){
  int nl, nd, sendRC, recvRC, sendP, recvP, npg, ncg;
  int max_sendRC = 1, max_recvRC = 1, max_sendP = 1, max_recvP = 1;
  int max1 = 0, max2 = 0, max3 = 0, maxCG = 1;

  GridS *pG;

  maxND = 1;
  for (nl = 0; nl < (pM->NLevels); nl++) {
    maxND = MAX(maxND, pM->DomainsPerLevel[nl]);
  }
  if ((start_addrP = (int*)calloc_1d_array(maxND, sizeof(int))) == NULL) {
    ath_error("[SMR_init]:Failed to allocate start_addrP\n");
  }

/* Loop over all parent Grids of Grids on this processor to find maximum total
 * number of words communicated */

  for (nl = 0; nl < (pM->NLevels); nl++) {
    for (nd = 0; nd < (pM->DomainsPerLevel[nl]); nd++) {
      sendRC = 0;
      recvRC = 0;
      sendP  = 1;
      recvP  = 1;

      if (pM->Domain[nl][nd].Grid != NULL) { /* there is a Grid on this proc */
        pG = pM->Domain[nl][nd].Grid;          /* set pointer to Grid */

        for (npg = 0; npg < pG->NPGrid; npg++) {
          sendRC += (pG->PGrid[npg].nWordsRC/NVAR)*(1+NSCALARS);
          recvP  += (pG->PGrid[npg].nWordsP/NVAR)*NION;
        }
        for (ncg = 0; ncg < pG->NCGrid; ncg++) {
          recvRC += (pG->CGrid[ncg].nWordsRC/NVAR)*(1+NSCALARS);
          sendP  += (pG->CGrid[ncg].nWordsP/NVAR)*NION;
        }

        max_sendRC = MAX(max_sendRC, sendRC);
        max_recvRC = MAX(max_recvRC, recvRC);
        max_sendP  = MAX(max_sendP, sendP);
        max_recvP  = MAX(max_recvP, recvP);
        max1  = MAX(max1, (pG->Nx[0]));
        max2  = MAX(max2, (pG->Nx[1]));
        max3  = MAX(max3, (pG->Nx[2]));
        maxCG = MAX(maxCG, pG->NCGrid);
      }
    }
  }

/* Allocate memory for send/receive buffers and EMFs used in RestrictCorrect */

  if ((send_bufRC =
         (double**)calloc_2d_array(maxND, max_sendRC,
                                   sizeof(double))) == NULL) {
    ath_error("[SMR_init]:Failed to allocate send_bufRC\n");
  }

#ifdef MPI_PARALLEL
  if ((recv_bufRC =
         (double***)calloc_3d_array(2, maxND, max_recvRC,
                                    sizeof(double))) == NULL) {
    ath_error("[SMR_init]: Failed to allocate recv_bufRC\n");
  }
  if ((recv_rq = (MPI_Request**)
                 calloc_2d_array(maxND, maxCG,
                                 sizeof(MPI_Request))) == NULL) {
    ath_error("[SMR_init]: Failed to allocate recv MPI_Request array\n");
  }
  if ((send_rq = (MPI_Request**)
                 calloc_2d_array(maxND, maxCG, sizeof(MPI_Request))) == NULL) {
    ath_error("[SMR_init]: Failed to allocate send MPI_Request array\n");
  }
#endif /* MPI_PARALLEL */

/* Allocate memory for send/receive buffers and GZ arrays used in Prolongate */

  if ((send_bufP =
         (double**)calloc_2d_array(maxND, max_sendP, sizeof(double))) == NULL) {
    ath_error("[SMR_init]:Failed to allocate send_bufP\n");
  }

  if ((recv_bufP =
         (double**)calloc_2d_array(maxND, max_recvP,
                                   sizeof(double))) == NULL) {
    ath_error("[SMR_init]: Failed to allocate recv_bufP\n");
  }

  max1 += 2*nghost;
  max2 += 2*nghost;
  max3 += 2*nghost;

  if ((GZ[0] = (Real***)calloc_3d_array(max3, max2, nghost, sizeof(Real)))
      == NULL) {
    ath_error("[SMR_init]:Failed to allocate GZ[0]C\n");
  }
  if ((GZ[1] = (Real***)calloc_3d_array(max3, nghost, max1, sizeof(Real)))
      == NULL) {
    ath_error("[SMR_init]:Failed to allocate GZ[1]C\n");
  }
  if ((GZ[2] = (Real***)calloc_3d_array(nghost, max2, max1, sizeof(Real)))
      == NULL) {
    ath_error("[SMR_init]:Failed to allocate GZ[2]C\n");
  }

  return;
}

void SMR_ionrad_destruct(){
  int i;

  if (start_addrP != NULL) {
    free_1d_array(start_addrP);
  }
  if (send_bufRC != NULL) {
    free_2d_array(send_bufRC);
  }
#ifdef MPI_PARALLEL
  if (recv_bufRC != NULL) {
    free_3d_array(recv_bufRC);
  }
  if (recv_rq != NULL) {
    free_2d_array(recv_rq);
  }
  if (send_rq != NULL) {
    free_2d_array(send_rq);
  }
#endif
  if (send_bufP != NULL) {
    free_2d_array(send_bufP);
  }
  if (recv_bufP != NULL) {
    free_2d_array(recv_bufP);
  }
  for (i = 0; i < 3; i++) {
    if (GZ[i] != NULL) {
      free_3d_array(GZ[i]);
    }
  }

  return;
}

void ProFlx(const Real Fi,
            const Real Fim1, const Real Fip1,
            const Real Fjm1, const Real Fjp1,
            const Real Fkm1, const Real Fkp1, Real PFlx[][2][2]){
  int i, j, k;

  /* Copy cell flux to finer grids */
  /* Finer grid's L-edge should equal parent cell's flux */
  for (k = 0; k < 2; k++) {
    for (j = 0; j < 2; j++) {
      for (i = 0; i < 2; i++) {
        PFlx[k][j][i] = Fi;
      }
    }
  }

  return;
}
#undef NION
#endif /* STATIC_MESH_REFINEMENT */
