#include "../copyright.h"
#define IONRAD_C
/*==============================================================================
 * FILE: ionrad.c
 *
 * PURPOSE: Contains function to control ionizing radiative transfer routines.
 *
 * CONTAINS PUBLIC FUNCTIONS:
 *   ion_radtransfer_init() - sets pointer to appropriate ionizing
 *                            radiative transfer function
 *   ion_radtransfer_init_domain() - sets domain information for ionizing
 *                                   radiative transfer module
 *============================================================================*/

#include <stdio.h>
#include <stdlib.h>
#include "../defs.h"
#include "../athena.h"
#include "../prototypes.h"
#include "prototypes.h"
#include "ionrad.h"

#ifdef ION_RADIATION
static int dim = 0;

void init_ionrad(MeshS *pM){
#ifdef STATIC_MESH_REFINEMENT
  SMR_ionrad_init(pM);
#endif

  /* Calcualte the dimensionality and error check */
  dim = 0;
  if (pM->Nx[0] > 1) {
    dim++;
  }
  if (pM->Nx[1] > 1) {
    dim++;
  }
  if (pM->Nx[2] > 1) {
    dim++;
  }

  switch (dim) {
  case 1:
    break;
  case 2:
    break;
  case 3:
#ifdef ION_RADPOINT
    ion_radpoint_init_3d(pM);
#endif
#ifdef ION_RADPLANE
    ion_radplane_init_3d(pM);
#endif
    return;
  default:
    ath_error("[init_ionrad]: Unsupported dim. Nx[0]=%d, Nx[1]=%d, Nx[2]=%d\n",
              pM->Nx[0], pM->Nx[1], pM->Nx[2]);
  }
}

void integrate_ionrad_init(MeshS *pM) {
  switch (dim) {
  case 1:
    return;
  case 2:
    return;
  case 3:
    integrate_ionrad_init_3d(pM);
    return;
  }
}

void integrate_ionrad(MeshS *pM){
  GridS *pG;
  int nl, nd;

#if defined(MPI_PARALLEL) && defined(STATIC_MESH_REFINEMENT)
  int ierr;
  Real dt, dt_glob;
#endif

  for (nl = 0; nl < pM->NLevels; nl++) {
    for (nd = 0; nd < pM->DomainsPerLevel[nl]; nd++) {
      if (pM->Domain[nl][nd].Grid != NULL) {
        pG = pM->Domain[nl][nd].Grid;
        pG->dt = pM->dt;
        /* Do radtransfer, can change Grid dt */
        ion_radtransfer_3d(&(pM->Domain[nl][nd]));
        if (pM->Domain[nl][nd].Level == 0) {
          pM->dt = MIN(pM->dt, pG->dt);
        }
      }
    }
#if defined(MPI_PARALLEL) && defined(STATIC_MESH_REFINEMENT)
    /* Get dt from procs that did root domain */
    if (nl == 0) {
      dt   = pM->dt;
      ierr = MPI_Allreduce(&dt, &dt_glob, 1, MP_RL, MPI_MIN, MPI_COMM_WORLD);
      if (ierr) {
        ath_perr(-1, "[integrate_ionrad]: MPI_Allreduce error = %d\n", ierr);
      }
      pM->dt = dt_glob;
    }
#endif
  }

  return;
}

/*----------------------------------------------------------------------------*/
/*! \fn void integrate_ionrad_destruct()
 *  \brief Free memory
 */
void integrate_ionrad_destruct(){
#if defined(STATIC_MESH_REFINEMENT)
  SMR_ionrad_destruct();
#endif

  switch (dim) {
  case 1:
    return;
  case 2:
    return;
  case 3:
    integrate_ionrad_destruct_3d();
    return;
  default:
    ath_error("[integrate_destruct]: Grid dimension = %d\n", dim);
  }
}
#endif /* ION_RADIATION */
