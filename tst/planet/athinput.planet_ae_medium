<comment>
problem = Adiabatic hydrostatic eqiulibrium atmosphere
author  = John McCann
config  = --with-problem=planet_ae --with-gas=hydro --enable-ion-radiation --enable-ion-plane --with-flux=roe --enable-h-correction --enable-fofc --with-nscalars=1 --enable-mpi --enable-smr
notes   = This test file is for the 75x50x50 Rp domain discussed in McCann et al. 2018. Planet parameters may be altered but might require different resolution.

<job>
problem_id         = hse                         # problem ID: basename of output filenames
maxout             = 2                           # Output blocks number from 1 -> maxout
num_domains        = 5                           # number of Domains in Mesh

<time>
cour_no            = 0.4                         # The Courant, Friedrichs, & Lewy (CFL) Number
nlim               = 10000000000000              # cycle limit
tlim               = 2.001e6

<output1>
out_fmt            = rst                         # History data dump
dt                 = 1.0e4                       # time increment between outputs

<output2>
out_fmt            = vtk                         # vtk image
out                = cons
num_include        = 12
include_expr_1     = tau
include_expr_2     = temp
include_expr_3     = flux
include_expr_4     = edot_HI_lya
include_expr_5     = edot_HI_recomb
include_expr_6     = edot_HI_ion
include_expr_7     = edot_pdV
include_expr_8     = edot_advect
include_expr_9     = ndot_HI_recomb
include_expr_10    = ndot_HI_ion
include_expr_11    = ndot_HI_advect
include_expr_12    = cell_dt
dt                 = 1.0e4

<domain1>
level              = 0                           # refinement level this Domain (root=0)
Nx1                = 300                         # Number of zones in X1-direction
x1min              = -7.5e11                     # minimum value of X
x1max              = 3.75e11                     # maximum value of X
bc_ix1             = 2                           # boundary condition flag for inner-I (X1)
bc_ox1             = 2                           # boundary condition flag for outer-I (X1)
NGrid_x1           = 3                           # with MPI, number of Grids in X1 coordinate
Nx2                = 200                         # Number of zones in X2-direction
x2min              = -3.75e11                     # minimum value of X2
x2max              = 3.75e11                      # maximum value of X2
bc_ix2             = 2                           # boundary condition flag for inner-J (X2)
bc_ox2             = 2                           # boundary condition flag for outer-J (X2)
NGrid_x2           = 3                           # with MPI, number of Grids in X1 coordinate
Nx3                = 200                         # Number of zones in X3-direction
x3min              = -3.75e11                    # minimum value of X3
x3max              = 3.75e11                     # maximum value of X3
bc_ix3             = 2                           # boundary condition flag for inner-K (X3)
bc_ox3             = 2                           # boundary condition flag for outer-K (X3)
NGrid_x3           = 3                           # with MPI, number of Grids in X1 coordinate
AutoWithNProc      = 0                           # Default Value

<domain2>
level              = 1                           # refinement level this Domain (root=0)
Nx1                = 32                          # Number of zones in X1-direction
Nx2                = 32                          # Number of zones in X2-direction
Nx3                = 32                          # Number of zones in X3-direction
iDisp              = 384                         # i-displacement measured in cells of this level
jDisp              = 184                         # j-displacement measured in cells of this level
kDisp              = 184                         # k-displacement measured in cells of this level
NGrid_x1           = 2
NGrid_x2           = 2
NGrid_x3           = 2
AutoWithNProc      = 0                           # Default Value

<domain3>
level              = 2                           # refinement level this Domain (root=0)
Nx1                = 56                          # Number of zones in X1-direction
Nx2                = 56                          # Number of zones in X2-direction
Nx3                = 56                          # Number of zones in X3-direction
iDisp              = 772                         # i-displacement measured in cells of this level
jDisp              = 372                         # j-displacement measured in cells of this level
kDisp              = 372                         # k-displacement measured in cells of this level
NGrid_x1           = 2
NGrid_x2           = 2
NGrid_x3           = 2
AutoWithNProc      = 0                           # Default Value

<domain4>
level              = 3                           # refinement level this Domain (root=0)
Nx1                = 96                          # Number of zones in X1-direction
Nx2                = 96                          # Number of zones in X2-direction
Nx3                = 96                          # Number of zones in X3-direction
iDisp              = 1552                        # i-displacement measured in cells of this level
jDisp              = 752                        # j-displacement measured in cells of this level
kDisp              = 752                         # k-displacement measured in cells of this level
NGrid_x1           = 2
NGrid_x2           = 2
NGrid_x3           = 2
AutoWithNProc      = 0                           # Default Value

<domain5>
level              = 4                           # refinement level this Domain (root=0)
Nx1                = 160                         # Number of zones in X1-direction
Nx2                = 160                         # Number of zones in X2-direction
Nx3                = 160                         # Number of zones in X3-direction
iDisp              = 3120                        # i-displacement measured in cells of this level
jDisp              = 1520                        # j-displacement measured in cells of this level
kDisp              = 1520                        # k-displacement measured in cells of this level
NGrid_x1           = 4
NGrid_x2           = 4
NGrid_x3           = 4
AutoWithNProc      = 0                           # Default Value

<domain6>
level              = 5                           # refinement level this Domain (root=0)
Nx1                = 192                         # Number of zones in X1-direction
Nx2                = 192                         # Number of zones in X2-direction
Nx3                = 192                         # Number of zones in X3-direction
iDisp              = 3104                        # i-displacement measured in cells of this level
jDisp              = 1504                        # j-displacement measured in cells of this level
kDisp              = 1504                        # k-displacement measured in cells of this level
NGrid_x1           = 4
NGrid_x2           = 4
NGrid_x3           = 4
AutoWithNProc      = 0                           # Default Value

<constants>
Ggrav              = 6.67408e-8                  # Newton's gravitational constant (NIST)
k_B                = 1.38064852e-16              # Boltzmann constant (NIST)
m_HI               = 1.673528e-24                # mass of HI (1.007825 amu)
m_HII              = 1.672621898e-24             # mass of HII (proton) (NIST)
m_HeI              = 6.646448e-24                # mass of HeI (4.002602 amu)
m_HeII             = 6.645566e-24                # mass of HeII (4.002053 amu)
m_HeIII            = 6.644657230e-24             # mass of HeIII (alpha particle) (NIST)
m_ZI               = 1.994473e-23                # mass of CI (12.011 amu)
m_ZII              = 1.994382e-23                # mass of CII (12.01045 amu)

<problem>
gamma              = 1.666666666667
# planet
#rmask              = 0.703125
#rib                = 0.781250
#rob               = 1.10603
# star and planet
#rmask             = 0.781250
#rib               = 0.859375
#rob               = 1.04851

# rotating frame
rmask             = 0.6875
rib               = 0.765625
rob                = 1.0724

#rlow               = 3.500000
#Rz1                = 3.556307

#dxMIN              = 0.015625
#dxMAX              = 0.50

r1    = 1.1125
r2    = 3.4642
rho1  = 9.68184e-20
rho2  = 1.68143e-23
E1    = 7.2688e-8
E2    = 3.23166e-9

r0                 = 1.5e10
Mp                 = 0.5e30
T0                 = 1.1e3

S_rmask            = 4.0e11
S_rib              = 4.3e11
Ms                 = 1.989e33                    # stellar mass (1.989e33 = 1.  M_\odot)
adist              = 10.e11                      # semi-major axis (7.48e11 = 0.05 AU)
ns                 = 3.5e4                       # number density of stellar wind @ S_rmask
vs                 = 2.0e7                       # radial velocity of stellar wind @ S_rmask
Ts                 = 1.35e6                      # temperature of stellar wind @ S_rmask
Bs                 = 1.0e-2                      # magnetic stellar feild (not yet implimented)

#delete later
flux               = 2.e13
nradplanes         = 1

<ionradiation>
flux               = 2.e13
nradplanes         = 1

sigma_pHI          = 4.087430e-18                # 16eV
e_gamma_HI         = 3.844800e-12                # 16eV
sigma_pHeI         = 0.0
e_gamma_HeI        = 0.0
sigma_pHeII        = 0.0
e_gamma_HeII       = 0.0
sigma_pZI          = 0.0
e_gamma_ZI         = 0.0

#delete later
e_gamma            = 3.844800e-12                # 16eV
sigma_ph           = 4.087430e-18                # 16eV
alpha_C            = 1.0e-10
k_B                = 1.38e-16

time_unit          = 1.0
max_de_iter        = 0.1
max_de_therm_iter  = 0.1
max_dx_iter        = 0.1
max_de_step        = 10.0
max_de_therm_step  = 10.0
max_dx_step        = 10.0
tfloor             = 10.0
tceil              = 10e8
maxiter            = 100000000.

Ggrav              = 6.67408e-8                  # Newton's gravitational constant (NIST)
k_B                = 1.38064852e-16              # Boltzmann constant (NIST)
m_H                = 1.673528e-24                # mass of HI (1.007825 amu)
mu                 = 1.673528e-24                # mass of HI (1.007825 amu)
