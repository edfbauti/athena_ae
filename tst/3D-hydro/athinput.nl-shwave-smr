<comment>
problem = non-linear non-axisymmetric shearing wave test
author  = T. Heinemann
journal = ApJ 626, 978 (2005)
config  = --with-problem=hgb --with-nscalars=1 --enable-mpi --enable-smr --enable-shearing-box --with-integrator=vl --enable-fofc --with-gas=hydro --with-flux=roe --enable-h-correction --with-order=3p

<job>
problem_id      = nl-shwave  # problem ID: basename of output filenames
maxout          = 2          # Output blocks number from 1 -> maxout
num_domains     = 2          # number of Domains in Mesh

<output1>
out_fmt = hst                # History data dump
dt      = 0.01

<output2>
out_fmt = bin                # Binary data dump
dt      = 0.01

<time>
cour_no         = 0.3         # The Courant, Friedrichs, & Lewy (CFL) Number
nlim            = 10000000    # cycle limit
tlim            = 16.0        # time limit (100*(1/Omega))

<domain1>
level           = 0         # refinement level this Domain (root=0)
Nx1             = 128       # Number of zones in X-direction
x1min           = -0.5      # minimum value of X
x1max           = 0.5       # maximum value of X
bc_ix1          = 4         # boundary condition flag for inner-I (X1)
bc_ox1          = 4         # boundary condition flag for outer-I (X1)
NGrid_x1        = 2         # with MPI, number of Grids in X1 coordinate

Nx2             = 512       # Number of zones in Y-direction
x2min           = -2.0      # minimum value of Y
x2max           = 2.0       # maximum value of Y
bc_ix2          = 4         # boundary condition flag for inner-J (X2)
bc_ox2          = 4         # boundary condition flag for outer-J (X2)
NGrid_x2        = 2         # with MPI, number of Grids in X1 coordinate

Nx3             = 32         # Number of zones in X3-direction
x3min           = -0.25     # minimum value of X3
x3max           = 0.25      # maximum value of X3
bc_ix3          = 4         # boundary condition flag for inner-K (X3)
bc_ox3          = 4         # boundary condition flag for outer-K (X3)
NGrid_x3        = 1         # with MPI, number of Grids in X1 coordinate

<domain2>
level           = 1         # refinement level this Domain (root=0)
Nx1             = 128       # Number of zones in X1-direction
Nx2             = 512       # Number of zones in X2-direction
Nx3             = 32        # Number of zones in X3-direction
iDisp           = 64        # i-displacement measured in cells of this level
jDisp           = 256       # j-displacement measured in cells of this level
kDisp           = 16        # k-displacement measured in cells of this level
NGrid_x1        = 2         # with MPI, number of Grids in X1 coordinate
NGrid_x2        = 2         # with MPI, number of Grids in X1 coordinate
NGrid_x3        = 1         # with MPI, number of Grids in X1 coordinate

<domain3>
level           = 2         # refinement level this Domain (root=0)
Nx1             = 128       # Number of zones in X1-direction
Nx2             = 512       # Number of zones in X2-direction
Nx3             = 32        # Number of zones in X3-direction
iDisp           = 192       # i-displacement measured in cells of this level
jDisp           = 768       # j-displacement measured in cells of this level
kDisp           = 48        # k-displacement measured in cells of this level
NGrid_x1        = 1         # with MPI, number of Grids in X1 coordinate
NGrid_x2        = 2         # with MPI, number of Grids in X1 coordinate
NGrid_x3        = 1         # with MPI, number of Grids in X1 coordinate

<problem>
iso_csound      = 1.0        # Isothermal sound speed squared
gamma           = 1.66666667 # Specific heat ratios
pres            = 1.0        # Pressure
omega           = 1.0        # Orbital frequency
qshear          = 1.5        # Logarithmic shear rate
amp             = 0.2        # initial amplitude, in units of Cs
ipert           = 7          # Initial condition number
nwx             = -2         # wavelengths in Lx
nwy             = 1          # wavelengths in Ly
nwz             = 0          # wavelengths in Lx
